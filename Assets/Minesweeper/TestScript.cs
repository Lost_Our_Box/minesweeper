﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public struct TestStruct
{
    int integer; //-1,0,1,2...
    float floatingPoint; //0.5548
    double doublePoint;
    decimal twiceDouble;
}

public class TestScript : MonoBehaviour
{
    int integer; //-1,0,1,2...
    float floatingPoint = 3.4f; //0.5548
    double doublePoint;
    decimal twiceDouble;

    char singleCharacter = 'a';
    string sentence;
    bool trueFalse;
    int[] intArray = new int[8];

    List<int> intList = new List<int>();
    Dictionary<int, string> intStringDict = new Dictionary<int, string>();

    // Start is called before the first frame update
    void Start()
    {
        intStringDict.Add(12, "Hello");
        intStringDict.Add(-1, "Hello");
        intStringDict.Add(50, "Hello");
        intStringDict.Add(6, "Hello");
        intList.Add(8);

        int value1 = intList[0];
        Debug.Log(intStringDict[0]);

        if (trueFalse ^ floatingPoint<0)
            Debug.Log("It's true");
        else if(floatingPoint > 0)
        {
            Debug.Log("It's not true");
            Debug.Log("It's biggger");
        }
        else
        {
            Debug.Log("Neither is true");
        }

        int counter = 0;
        switch (counter)
        {
            case 0:
                Debug.Log("0");
                break;
            case 2:
            case 3:
            case 4:
                Debug.Log("stuff");
                break;
            default:
                Debug.Log("default");
                break;
        }

        while(!trueFalse)
        {
            Debug.Log("printing...");
            break;
        }

        do
        {
            Debug.Log("printing...");
        } while (!trueFalse);

        for ( int i = 0; i>10; i++)
        {
            print(i);
        }

        Debug.Log("Done");

        foreach ( int eachInteger in intList)
        {
            print(eachInteger);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
