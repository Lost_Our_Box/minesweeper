﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombGridPosition : GridPositionBase
{
    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        myType = GridType.BOMB;
        OnNormalPress = new ExplodingPress();
        Debug.Log("Bomb Mask: " + manager.bombMask);
        gameObject.layer = LayerMask.NameToLayer("Bomb");
    }

}
