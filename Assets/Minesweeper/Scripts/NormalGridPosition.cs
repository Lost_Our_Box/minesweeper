﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalGridPosition : GridPositionBase
{

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        myType = GridType.NOT_BOMB;
        OnNormalPress = new CountingPress();
    }

}
