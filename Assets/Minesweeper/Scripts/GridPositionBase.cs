﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GridPositionBase : MonoBehaviour //, iPressable
{

    public enum GridType { BOMB, NOT_BOMB}
    public enum PositionState {
        UP = 1,
        PRESSED = 2,
        FLAG = 3,
        BOOM  = 4
    } // use state machine states as pressables

    public GameManager manager;

    protected GridType myType;
    public PositionState myState;
    public iPressable OnNormalPress;
    public iPressable OnRightPress;


    // Start is called before the first frame update
    virtual protected void Start()
    {
        myState = PositionState.UP;
        OnRightPress = new RightPress();
    }

    /*
    public void OnPress()
    {
        if (myState == PositionState.PRESSED)
        {

        }
        else if ( myState == PositionState.UP)
        {

        }
        else if (myState == PositionState.FLAG)
        {

        }
        else if (myState == PositionState.BOOM)
        {

        }

    }
    */

    protected void OnMouseDown()
    {
        OnNormalPress = OnNormalPress.OnPress(this);
        manager.CountBombs();
    }


    public void OnRightClick()
    {
        OnRightPress = OnRightPress.OnPress(this);
    }


    public static GridPositionBase Get(float bombProbability, GameObject newGridPos)
    {
        if (Random.Range(0, 1f) < bombProbability)
            return newGridPos.AddComponent<BombGridPosition>();
        else
            return newGridPos.AddComponent<NormalGridPosition>();
    }

}
