﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iPressable
{
    iPressable OnPress(GridPositionBase myPosition);
}

public class EmptyPress : iPressable
{
    public iPressable OnPress(GridPositionBase myPosition)
    {
        Debug.Log("Empty Press");
        return this;
    }
}


public class CountingPress : iPressable
{
    public iPressable OnPress(GridPositionBase myPosition)
    {
        Debug.Log("Counting Press");

        myPosition.OnRightPress = new EmptyPress();
        myPosition.myState = GridPositionBase.PositionState.PRESSED;

        //Count the number of bombs around it then become unpressable
        Collider[] colliders = new Collider[8];
        int overLapping = Physics.OverlapBoxNonAlloc(myPosition.transform.position, 
            new Vector3(1,.5f,1), 
            colliders, 
            new Quaternion(), 
            myPosition.manager.bombMask);

        myPosition.transform.GetChild(0).gameObject.SetActive(false);
        myPosition.transform.GetChild(1).gameObject.SetActive(true);
        myPosition.transform.GetChild(1).GetComponent<TMPro.TextMeshPro>().text = overLapping.ToString();

        Debug.Log("Overlapped with " + overLapping);

        return new EmptyPress();
    }
}


public class ExplodingPress : iPressable
{
    public iPressable OnPress(GridPositionBase myPosition)
    {
        Debug.Log("Exploding Press");
        //Explode, signal gameover, become unpressable
        myPosition.myState = GridPositionBase.PositionState.BOOM;
        myPosition.OnRightPress = new EmptyPress();

        myPosition.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = myPosition.manager.bombSprite;
        myPosition.transform.GetChild(0).gameObject.SetActive(true);
        myPosition.manager.OnLose(myPosition.transform.position);
        return new EmptyPress();
    }
}


public class RightPress : iPressable
{
    public iPressable OnPress(GridPositionBase myPosition)
    {
        Debug.Log("OnRightPress");
        //change the state, change the 
        GameObject checkMark = myPosition.transform.GetChild(0).gameObject;
        checkMark.SetActive(!checkMark.activeSelf);
        myPosition.myState = checkMark.activeSelf?GridPositionBase.PositionState.FLAG:GridPositionBase.PositionState.UP;
        return this;
    }
}
