﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    GameObject GridPrefab;

    [SerializeField]
    TMPro.TextMeshProUGUI liveCellDisplay;

    [SerializeField]
    Vector2 size;

    [SerializeField]
    [Range(0, 1f)]
    float bombPercentage;

    public Sprite bombSprite;

    public LayerMask bombMask;

    //Array of Grids
    List<GridPositionBase> gridPositions;

    Ray ray;
    RaycastHit hit;


    // Start is called before the first frame update
    
        private void Start()
        {
            StartCoroutine(Initialize());
        }

        IEnumerator Initialize()
        {
            CreateBoard();
            yield return new WaitForEndOfFrame();
            CountBombs();
        }
    
    
        public void Restart()
        {
            foreach (GridPositionBase gp in gridPositions)
                Destroy(gp.gameObject);

            StartCoroutine(Initialize());
        }
    
    
        private void Update()
        {
            if (Input.GetButtonDown("Fire2"))
            {
                ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                if (Physics.Raycast(ray, out hit))
                {
                    GridPositionBase hitPosition = hit.collider.GetComponent<GridPositionBase>();
                    if (hitPosition)
                    {
                        hitPosition.OnRightClick();
                        CountBombs();
                    }
                }
            }
        }
    
    
        public void CreateBoard()
        {
            gridPositions = new List<GridPositionBase>();
            for(int rows = 0; rows<size.x; rows++)
            {
                for(int columns = 0; columns<size.y; columns++)
                {
                    Vector3 newPosition = transform.position + new Vector3(rows - size.x / 2 +.025f*rows, 0, columns - size.x / 2+.025f*columns);
                    GameObject newGridPos = Instantiate(GridPrefab, newPosition, new Quaternion(), transform);

                    GridPositionBase gridPosition = GridPositionBase.Get(bombPercentage, newGridPos);
                    gridPosition.manager = this;
                    gridPositions.Add(gridPosition);
                }
            }
        }
    
       public void CountBombs()
       {
           int flags = 0;// gridPositions.Count;
           int bombs = 0;
           int liveCells = 0;
           foreach (GridPositionBase gridPosition in gridPositions)
           {
               bool isBomb = gridPosition.GetType() == typeof(BombGridPosition);
               if (isBomb) {
                   bombs++;
               }

               else if (gridPosition.myState.HasFlag(GridPositionBase.PositionState.UP))
               {
                   liveCells++;
               }

               if (gridPosition.myState == GridPositionBase.PositionState.FLAG)
                   flags++;

           }

           liveCellDisplay.text = "Bombs Remaining: " + (bombs - flags) + "\nLive Cells Remaining: " + liveCells;
           if (liveCells == 0)
               StartCoroutine(OnWin());
       }
 
    
        public IEnumerator OnWin()
        {
            Debug.Log("You Win!");
            foreach (GridPositionBase gridPosition in gridPositions)
            {
                bool isBomb = gridPosition.GetType() == typeof(BombGridPosition);
                if (isBomb)
                {
                    gridPosition.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.up * 50, gridPosition.transform.position + Vector3.right * .2f, ForceMode.Impulse);
                    yield return new WaitForSeconds(1);
                }
            }
        }
    
        public void OnLose(Vector3 position)
        {
            foreach(GridPositionBase gridPosition in gridPositions)
            {
                Rigidbody myRigidbody = gridPosition.GetComponent<Rigidbody>();
                myRigidbody.AddExplosionForce(500, position, 10);
            }
        }
}
