﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUnit : MonoBehaviour
{
    [SerializeField]
    protected int health;
    [SerializeField]
    protected int moveRange;
    [SerializeField]
    protected int attackRange;
    [SerializeField]
    protected int attackDamage;
    [SerializeField]
    protected GridTile location;

    protected bool isMoving;

    [SerializeField]
    protected GameManagerTBS manager;

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
            Die();
    }

    public abstract void Attack(BaseUnit target);
    public abstract void Move(GridTile newLocation);
    protected abstract void Die();

    protected IEnumerator GoTo(GridTile destination)
    {
        while (Vector3.Distance(destination.transform.position, transform.position) > .05f)
        {
            transform.position = Vector3.Lerp(transform.position,
                destination.transform.position, Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        location = destination;
        isMoving = false;
    }

    public void OnMouseDown()
    {
        manager.SelectUnit(this);
    }


}
