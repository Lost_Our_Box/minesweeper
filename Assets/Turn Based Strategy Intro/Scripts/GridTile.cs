﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTile : MonoBehaviour
{
    public List<GridTile> neighbors;
    public LayerMask tileLayer;
    public GameManagerTBS manager;

    // Start is called before the first frame update
    void Start()
    {
        neighbors = new List<GridTile>();
        Collider[] foundneighbors = Physics.OverlapBox(transform.position, new Vector3(.75f, .75f, .75f), new Quaternion(), tileLayer);
        foreach(Collider neighbor in foundneighbors)
        {
            GridTile tile = neighbor.GetComponent<GridTile>();
            if(tile != this)
            {
                neighbors.Add(tile);
            }
        }
    }

    public void OnMouseDown()
    {
        manager.MoveUnit(this);
    }
}
