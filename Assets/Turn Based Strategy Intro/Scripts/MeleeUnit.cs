﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeUnit : BaseUnit
{
    public override void Attack(BaseUnit target)
    {
        if(Vector3.Distance(transform.position,target.transform.position)<=attackRange)
            target.TakeDamage(attackDamage);
    }

    public override void Move(GridTile newLocation)
    {
        //Astar.Move(location, newLocation);
        if (!isMoving)
        {
            isMoving = true;
            StartCoroutine(GoTo(newLocation));
        }
    }

    protected override void Die()
    {
        Destroy(gameObject);
    }
}
