﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTBS : MonoBehaviour
{
    public bool generateGrid;
    public Vector2 gridSize;
    public GameObject gridObject;
    public float buffer;

    public BaseUnit currentSelected;
    
    // Start is called before the first frame update
    void Start()
    {
        if (generateGrid)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                for (int y = 0; y < gridSize.y; y++)
                {
                    GameObject tile = Instantiate(gridObject,
                    new Vector3(gridSize.x / 2 + x + buffer * x, 0,
                    gridSize.y / 2 + y + buffer * y),
                    new Quaternion());

                    tile.GetComponent<GridTile>().manager = this;
                }
            }
        }
    }
    
    public void SelectUnit(BaseUnit unit)
    {
      
        if(!currentSelected || currentSelected.tag == unit.tag)
            currentSelected = unit;
        else
        {
            currentSelected.Attack(unit);
        }
    }

    public void MoveUnit(GridTile destination)
    {
        if(currentSelected)
        {
            currentSelected.Move(destination);
        }
    }
}
